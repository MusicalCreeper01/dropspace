﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Win32;
using System.Runtime.InteropServices;

namespace Dropspace
{
    public static class FileUtities
    {
        public static void SetIcon(this String file, string iconPath) {
            if (!Directory.Exists(file)) {
                throw new System.Exception("Invailed file path");
            }
            Console.WriteLine("begin");
            FileInfo fileInfo = new FileInfo(new FileInfo(file).FullName + @"\desktop.ini");
            FileAttributes fa = fileInfo.Attributes;
            FileStream fs = fileInfo.OpenWrite();
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("[.ShellClassInfo]");
            sw.WriteLine(@"IconResource=C:\Windows\system32\SHELL32.dll,46");
            sw.Flush();
            sw.Close();
            fs.Close();
            fileInfo.Attributes = FileAttributes.Hidden | FileAttributes.Archive | FileAttributes.System;
            Console.WriteLine("end");
            RefreshIconCache();
        }

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern IntPtr SendMessageTimeout(
            int windowHandle,
            int Msg,
            int wParam,
            String lParam,
            SendMessageTimeoutFlags flags,
            int timeout,
            out int result);
        [Flags]
        enum SendMessageTimeoutFlags : uint
        {
            SMTO_NORMAL = 0x0,
            SMTO_BLOCK = 0x1,
            SMTO_ABORTIFHUNG = 0x2,
            SMTO_NOTIMEOUTIFNOTHUNG = 0x8
        }


        static void RefreshIconCache()
        {

            // get the the original Shell Icon Size registry string value
            RegistryKey k = Registry.CurrentUser.OpenSubKey("Control Panel").OpenSubKey("Desktop").OpenSubKey("WindowMetrics", true);
            Object OriginalIconSize = k.GetValue("Shell Icon Size");

            // set the Shell Icon Size registry string value
            k.SetValue("Shell Icon Size", (Convert.ToInt32(OriginalIconSize) + 1).ToString());
            k.Flush(); k.Close();

            // broadcast WM_SETTINGCHANGE to all window handles
            int res = 0;
            SendMessageTimeout(0xffff, 0x001A, 0, "", SendMessageTimeoutFlags.SMTO_ABORTIFHUNG, 5000, out res);
            //SendMessageTimeout(HWD_BROADCAST,WM_SETTINGCHANGE,0,"",SMTO_ABORTIFHUNG,5 seconds, return result to res)

            // set the Shell Icon Size registry string value to original value
            k = Registry.CurrentUser.OpenSubKey("Control Panel").OpenSubKey("Desktop").OpenSubKey("WindowMetrics", true);
            k.SetValue("Shell Icon Size", OriginalIconSize);
            k.Flush(); k.Close();

            SendMessageTimeout(0xffff, 0x001A, 0, "", SendMessageTimeoutFlags.SMTO_ABORTIFHUNG, 5000, out res);

        }

    }
}
