﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dropspace
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
        }

        private void buttonAddShortcut_Click(object sender, EventArgs e)
        {
            Program.appShortcutToDesktop();
        }

        private void buttonAddFavorites_Click(object sender, EventArgs e)
        {
            Program.addToFavorites();
        }

        
    }
}
