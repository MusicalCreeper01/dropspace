﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Dropspace.sharing;

namespace Dropspace.ui
{
    public partial class FileView : UserControl
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                this.BackColor = Color.Transparent;
                this.BorderStyle = IsSelected ? BorderStyle.FixedSingle : BorderStyle.None;
                this.BackColor = Color.Transparent;
            }
        }


        // Event fires when the MouseClick event fires for the UC or any of its child controls.
        public event EventHandler<EventArgs> WasClicked;

        private void UsersGrid_Load(object sender, EventArgs e)
        {
            // Register the MouseClick event with the UC's surface.
            this.MouseClick += Control_MouseClick;
            // Register MouseClick with all child controls.
            foreach (Control control in Controls)
            {
                control.MouseClick += Control_MouseClick;
            }
            foreach (Control control in Controls)
            {
                control.DoubleClick += event_DoubleClick;
            }
        }

        private void Control_MouseClick(object sender, MouseEventArgs e)
        {
            var wasClicked = WasClicked;
            if (wasClicked != null)
            {
                WasClicked(this, EventArgs.Empty);
            }
            // Select this UC on click.
            IsSelected = true;
            if (folderViewer != null)
                folderViewer.selectedFile = filePath;
        }

        public System.Windows.Media.ImageSource Icon;

        public string filePath = "file.png";

        public FileView(string path)
        {
            filePath = path;
            InitializeComponent();

            using (System.Drawing.Icon sysicon = System.Drawing.Icon.ExtractAssociatedIcon(filePath))
            {
                labelFileLogo.Image = sysicon.ToBitmap();    
            }
            labelFileName.Text = Path.GetFileName(filePath);
            this.Load += UsersGrid_Load;
            
        }

        public FolderView folderViewer;

        public FileView(FolderView fv, string path)
        {
            folderViewer = fv;
            filePath = path;
            InitializeComponent();
            FileAttributes attr = File.GetAttributes(path);
                
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory){
                SHFILEINFO shinfo = new SHFILEINFO();
                IntPtr hImgSmall = Win32.SHGetFileInfo(path, 0, ref shinfo,
                                               (uint)Marshal.SizeOf(shinfo),
                                                Win32.SHGFI_ICON |
                                                Win32.SHGFI_SMALLICON);

                //Use this to get the large Icon
                //hImgLarge = SHGetFileInfo(fName, 0,
                //ref shinfo, (uint)Marshal.SizeOf(shinfo),
                //Win32.SHGFI_ICON | Win32.SHGFI_LARGEICON);
                //The icon is returned in the hIcon member of the shinfo
                //struct
                System.Drawing.Icon myIcon =
                       System.Drawing.Icon.FromHandle(shinfo.hIcon);
                labelFileLogo.Image = myIcon.ToBitmap();
            
            }else{
                using (System.Drawing.Icon sysicon = System.Drawing.Icon.ExtractAssociatedIcon(filePath))
                {
                    labelFileLogo.Image = sysicon.ToBitmap();
                }
            }

            labelFileName.Text = Path.GetFileName(filePath);
            this.Load += UsersGrid_Load;

            if (fv != null)
            {
                this.MouseClick += fv.clickEvent_Click;
                foreach (Control control in Controls)
                {
                    control.MouseClick += fv.clickEvent_Click;
                }
            }
        }

        private void event_DoubleClick(object sender, EventArgs e)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = filePath;
            proc.StartInfo.UseShellExecute = true;
            proc.Start();   
        }

        private void googleDriveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Sharing.ShareGoogleDrive(filePath);
        }

        private void doprboxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sharing.ShareDropbox(filePath);
        }

        private void oneDriveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Sharing.ShareOneDrive(filePath);
        }

        private void imgurToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Sharing.ShareImgur(filePath);
        }

        private void youTubeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Sharing.ShareYouTube(filePath);
        }



        //Get Folder Icon
        [StructLayout(LayoutKind.Sequential)]
        public struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };

        class Win32
        {
            public const uint SHGFI_ICON = 0x100;
            public const uint SHGFI_LARGEICON = 0x0;    // 'Large icon
            public const uint SHGFI_SMALLICON = 0x1;    // 'Small icon

            [DllImport("shell32.dll")]
            public static extern IntPtr SHGetFileInfo(string pszPath,
                                        uint dwFileAttributes,
                                        ref SHFILEINFO psfi,
                                        uint cbSizeFileInfo,
                                        uint uFlags);
        }

    }
}
