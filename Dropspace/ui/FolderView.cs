﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Dropspace.ui
{
    public partial class FolderView : UserControl
    {
        //Fixed the background color clickering to default when updating control
        //http://stackoverflow.com/questions/2612487/how-to-fix-the-flickering-in-user-controls
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        } 

        public string selectedFile = "";

        public FolderView()
        {
            InitializeComponent();

            DirectoryInfo d = new DirectoryInfo(Program.DropspaceLocation);

            foreach (DirectoryInfo file in d.GetDirectories("*"))
            {
                flowLayoutPanel1.Controls.Add(new FileView(this, file.FullName));
            }

            foreach (FileInfo file in d.GetFiles("*"))
            {
                flowLayoutPanel1.Controls.Add(new FileView(this, file.FullName));
            }

            new Thread(() =>
            {
                Thread.Sleep(10000);
                refresh();
            });
        }

        public void refresh() {

            DirectoryInfo d = new DirectoryInfo(Program.DropspaceLocation);
            flowLayoutPanel1.Controls.Clear();
            foreach (FileInfo file in d.GetFiles("*"))
            {
                flowLayoutPanel1.Controls.Add(new FileView(this, file.FullName));
            }
        }

        public void UsersGrid_WasClicked(object sender, MouseEventArgs e)
        {
            // Set IsSelected for all UCs in the FlowLayoutPanel to false. 
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                if (c is FileView)
                {
                    ((FileView)c).IsSelected = false;
                }
            }
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;

        }
        private void file_DragDropped(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files) File.Move(file, Program.DropspaceLocation + @"\" + Path.GetFileName(file));
        }

        private void UsersGrid_WasClicked(object sender, EventArgs e)
        {
            // Set IsSelected for all UCs in the FlowLayoutPanel to false. 
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                if (c is FileView)
                {
                    ((FileView)c).IsSelected = false;
                }
            }
        }
        public void clickEvent_Click(object sender, EventArgs e)
        {
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                if (c is FileView)
                {
                    ((FileView)c).IsSelected = false;
                }
            }
        }

        private void flowLayoutPanel1_Resize(object sender, EventArgs e)
        {
            this.refresh();
        }
    }

    
}
