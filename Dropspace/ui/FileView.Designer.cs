﻿namespace Dropspace.ui
{
    partial class FileView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelFileName = new System.Windows.Forms.Label();
            this.contextMenuFile = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shareToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.googleDriveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oneDriveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.doprboxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imgurToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.youTubeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.labelFileLogo = new System.Windows.Forms.Label();
            this.contextMenuFile.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelFileName
            // 
            this.labelFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFileName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelFileName.Location = new System.Drawing.Point(3, 50);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(84, 40);
            this.labelFileName.TabIndex = 2;
            this.labelFileName.Text = "TestFile.png";
            this.labelFileName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // contextMenuFile
            // 
            this.contextMenuFile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.shareToolStripMenuItem1});
            this.contextMenuFile.Name = "contextMenuStrip1";
            this.contextMenuFile.Size = new System.Drawing.Size(105, 70);
            // 
            // moveToolStripMenuItem
            // 
            this.moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            this.moveToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.moveToolStripMenuItem.Text = "Move";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            // 
            // shareToolStripMenuItem1
            // 
            this.shareToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.googleDriveToolStripMenuItem1,
            this.oneDriveToolStripMenuItem1,
            this.doprboxToolStripMenuItem,
            this.imgurToolStripMenuItem1,
            this.youTubeToolStripMenuItem1});
            this.shareToolStripMenuItem1.Name = "shareToolStripMenuItem1";
            this.shareToolStripMenuItem1.Size = new System.Drawing.Size(104, 22);
            this.shareToolStripMenuItem1.Text = "Share";
            // 
            // googleDriveToolStripMenuItem1
            // 
            this.googleDriveToolStripMenuItem1.Image = global::Dropspace.Properties.Resources.Google_Drive_logo;
            this.googleDriveToolStripMenuItem1.Name = "googleDriveToolStripMenuItem1";
            this.googleDriveToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.googleDriveToolStripMenuItem1.Text = "Google Drive";
            this.googleDriveToolStripMenuItem1.Click += new System.EventHandler(this.googleDriveToolStripMenuItem1_Click);
            // 
            // oneDriveToolStripMenuItem1
            // 
            this.oneDriveToolStripMenuItem1.Enabled = false;
            this.oneDriveToolStripMenuItem1.Image = global::Dropspace.Properties.Resources.smallest_cloud;
            this.oneDriveToolStripMenuItem1.Name = "oneDriveToolStripMenuItem1";
            this.oneDriveToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.oneDriveToolStripMenuItem1.Text = "OneDrive (Coming soon)";
            this.oneDriveToolStripMenuItem1.Click += new System.EventHandler(this.oneDriveToolStripMenuItem1_Click);
            // 
            // doprboxToolStripMenuItem
            // 
            this.doprboxToolStripMenuItem.Enabled = false;
            this.doprboxToolStripMenuItem.Image = global::Dropspace.Properties.Resources.glyph_vflK_Wlfk;
            this.doprboxToolStripMenuItem.Name = "doprboxToolStripMenuItem";
            this.doprboxToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.doprboxToolStripMenuItem.Text = "Dropbox (Coming soon)";
            this.doprboxToolStripMenuItem.Click += new System.EventHandler(this.doprboxToolStripMenuItem_Click);
            // 
            // imgurToolStripMenuItem1
            // 
            this.imgurToolStripMenuItem1.Image = global::Dropspace.Properties.Resources.imgur_logo;
            this.imgurToolStripMenuItem1.Name = "imgurToolStripMenuItem1";
            this.imgurToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.imgurToolStripMenuItem1.Text = "Imgur";
            this.imgurToolStripMenuItem1.Click += new System.EventHandler(this.imgurToolStripMenuItem1_Click);
            // 
            // youTubeToolStripMenuItem1
            // 
            this.youTubeToolStripMenuItem1.Image = global::Dropspace.Properties.Resources.YouTube_icon_full_color;
            this.youTubeToolStripMenuItem1.Name = "youTubeToolStripMenuItem1";
            this.youTubeToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.youTubeToolStripMenuItem1.Text = "YouTube";
            this.youTubeToolStripMenuItem1.Click += new System.EventHandler(this.youTubeToolStripMenuItem1_Click);
            // 
            // labelFileLogo
            // 
            this.labelFileLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFileLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFileLogo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelFileLogo.Image = global::Dropspace.Properties.Resources.text_x_generic;
            this.labelFileLogo.Location = new System.Drawing.Point(3, 0);
            this.labelFileLogo.Name = "labelFileLogo";
            this.labelFileLogo.Size = new System.Drawing.Size(84, 40);
            this.labelFileLogo.TabIndex = 3;
            this.labelFileLogo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FileView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.ContextMenuStrip = this.contextMenuFile;
            this.Controls.Add(this.labelFileLogo);
            this.Controls.Add(this.labelFileName);
            this.Name = "FileView";
            this.Size = new System.Drawing.Size(90, 90);
            this.DoubleClick += new System.EventHandler(this.event_DoubleClick);
            this.contextMenuFile.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.ContextMenuStrip contextMenuFile;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shareToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem googleDriveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem oneDriveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem doprboxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imgurToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem youTubeToolStripMenuItem1;
        private System.Windows.Forms.Label labelFileLogo;
    }
}
