﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using Dropspace.sharing;
using IWshRuntimeLibrary;
using System.IO;
namespace Dropspace
{
    static class Program
    {


        public static string DropspaceLocation = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\DropSpace";
        public static Form1 MainForm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            RunSetup();
        }

        public static string appName = "DropSpace";

        public static void RunSetup()
        {
            Debug.WriteLine("Running setup");
            Debug.WriteLine("Initalizing registry keys");
            RegistryManager.init();

            if (RegistryManager.HasKey("FolderLocation"))
            {
                Debug.WriteLine("Registry contains folder location key");
                Program.DropspaceLocation = RegistryManager.GetKey("FolderLocation");
            }
            else if (Program.DropspaceLocation == "")
            {
                Debug.WriteLine("Registry does not contain folder location key");
                Program.DropspaceLocation = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\DropSpace";
                RegistryManager.Register("FolderLocation", Program.DropspaceLocation);
            }

            if (!Directory.Exists(Program.DropspaceLocation))
            {
                Directory.CreateDirectory(Program.DropspaceLocation);
                //Program.DropspaceLocation.SetIcon("file.png");
            }

            /*RegistryKey topLevel = Registry.CurrentUser;
            RegistryKey key = topLevel.OpenSubKey(
                @"Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders",
                true);

            string favoriteFolder = key.GetValue("Favorites").ToString();*/
            //string favoriteFolder = Environment.GetFolderPath(Environment.SpecialFolder.Favorites);

            Debug.WriteLine("Checking auth handlers");

            /*
            if (!RegistryManager.HasKey("DropboxAuth"))
            {
                Debug.WriteLine("Dropbox not authorized, showing auth window");
                new DropboxAuth();
            }
            else
            {
                Debug.WriteLine("Dropbox authorized, initalizing...");
                DropBox.init();
            }*/


            //if (!RegistryManager.HasKey("OneDriveAccessToken") && !RegistryManager.HasKey("RefreshToken"))
            //OneDrive.init();
            //else
            GoogleAuth.Authenticate();

            Imgur.init();

            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
            //Move to one-time installed later
            /* string menuCommand = string.Format("\"{0}\" \"%L\"",
                                    Application.ExecutablePath);
             FileShellExtension.Register("*", "Dropspace Context Menu",
                                         "Add To Dropspace", menuCommand);
             //---
             */

            MainForm = new Form1();


            Application.Run(MainForm);
        }

        static void OnProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("I'm out of here");

            //Move to one-time installed later
            // FileShellExtension.Unregister("*", "Dropspace Context Menu");
        }

        public static void appShortcutToDesktop()
        {
            string deskDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

            using (StreamWriter writer = new StreamWriter(deskDir + "\\" + appName + ".url"))
            {
                string app = System.Reflection.Assembly.GetExecutingAssembly().Location;
                writer.WriteLine("[InternetShortcut]");
                writer.WriteLine("URL=file:///" + app);
                writer.WriteLine("IconIndex=0");
                string icon = app.Replace('\\', '/');
                writer.WriteLine("IconFile=" + icon);
                writer.Flush();
            }
        }

        public static void addToFavorites(){
            string favoriteFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Links";
            Debug.WriteLine("Favorites folder: " + favoriteFolder);
            if (!System.IO.File.Exists(favoriteFolder + "\\DropSpace.lnk"))
            {
                Debug.WriteLine("Adding folder to favorites: " + favoriteFolder + "\\DropSpace.lnk");
                var desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                IWshShortcut shortcut;
                shortcut = (IWshShortcut)new WshShell().CreateShortcut(Path.Combine(favoriteFolder, @"DropSpace.lnk"));
                shortcut.TargetPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\DropSpace";
                shortcut.Save();

            }
        }


    }
}
