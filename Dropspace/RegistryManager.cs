﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Win32;

namespace Dropspace
{
    static class RegistryManager
    {

        // create path to registry location
        static string regPath = @"Software\\DropSpace";

        public static void init() {
            Debug.WriteLine("Current registry: " + Registry.CurrentUser.ToString());
            try
            {
                Registry.CurrentUser.OpenSubKey(regPath, true);

            }
            catch (System.TypeInitializationException ex)
            {
                Registry.CurrentUser.CreateSubKey(regPath);
            }
        }

        public static void Register(string shellKeyName, string value)
        {
            
            // add context menu to the registry
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(regPath, true))
            {
                key.SetValue(shellKeyName, value);
                Debug.WriteLine("Added key " + key.GetValue(shellKeyName).ToString() + " to registry");
            }
        }


        public static void UnRegister(string shellKeyName)
        {

            using (RegistryKey regkey = Registry.CurrentUser.OpenSubKey(regPath, true))
            {
                if (regkey.GetValue(shellKeyName) != null)
                {
                    regkey.DeleteValue(shellKeyName);
                }
            }
        }

        public static bool HasKey(string key) {
            using (RegistryKey regkey = Registry.CurrentUser.OpenSubKey(regPath, true))
            {
                if (regkey != null)
                {
                    if (regkey.GetValueNames() != null)
                    {
                        Debug.WriteLine("Registry " + key + ":" + regkey.GetValueNames().Contains(key));
                        return regkey.GetValueNames().Contains(key);
                        
                    }
                    else
                        return false;
                }
                else {
                    return false;
                }
            }
        }

        public static string GetKey(string shellKeyName)
        {

            using (RegistryKey regkey = Registry.CurrentUser.OpenSubKey(regPath, true))
            {
                string s = "";
                try
                {
                    if (regkey.GetValueNames().Contains(shellKeyName))
                    {
                        object o = regkey.GetValue(shellKeyName);
                        s = (string)o;
                        Debug.WriteLine("Got registry key: " + regkey.ToString() + ":" + s);
                    }

                }
                catch (System.TypeInitializationException ex) { }

                return s;
            }

        }
    }
}
