﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dropspace.Properties;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;

using Dropspace.ui;
using Dropspace.sharing;
namespace Dropspace
{
    public partial class Form1 : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public Form1()
        {
            InitializeComponent();
            this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;

            
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            //Hide();
            Application.Exit();
        }



        private void TrayMinimizerForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
                Hide();

        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void MaximizeButton_Click(object sender, EventArgs e)
        {
            if(this.WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (this.WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }

        private void toolBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Show();
        }

        

        private void doprboxToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public enum MessageType { 
            Upload,
            Info
        }

        public void PopupMessage(MessageType type, string message) {
            PopupMessage(type, message, "");
        }
        private static string lastLink = "";
        public void PopupMessage(MessageType type, string message, string link)
        {
            string title = "";
            if (type == MessageType.Upload)
            {
                title = "Upload Info";
                notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            } if (type == MessageType.Info)
            {
                title = "Info";
                notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            }
            notifyIcon1.BalloonTipTitle = title;
            notifyIcon1.BalloonTipText = message + " (link copied to clipboard)";
            notifyIcon1.ShowBalloonTip(5000);
            lastLink = link;

            Clipboard.SetText(link);
        }

        private void notifyIcon1_BallonClicked(object sender, EventArgs e)
        {
            if(lastLink != "" && lastLink != null)
                Process.Start(lastLink);
        }

        private void buttonOptions_Click(object sender, EventArgs e)
        {
            FormSettings form = new FormSettings();
            form.Show();
        }

        
    }
}
