# DropSpace #

### What is DropSpace ###

DropSpace is a program the creates a virtual second desktop with advanced sharing features.Using DropSpace you can keep the clutter off the desktop, and also easily share any file by just right clicking and selecting a sharing service

### Features ###

* A clutter-cleared desktop
* A virtual desktop where you can Drag-And-Drop files to
* Simple right-click sharing
* Sort your files into different categories (conning soon)

### Contribution help ###
**The Source code will not run**
Due to needing to ignore some files due to client secrets, the code is not complete and won't run as expected.

If you want a feature to extend the sharing APIs, you'll need to suggest it to me. Due to the use of OAuth client secrets, I cannot publish the source code for the sharing clients. (I had to regenerate all the secrets after accidentally committing them, that took awhile -_-)

### Notes about source code ###

The source code might **not** always work. At best, it is probably unstable most of the time. I try to **only** commit when the change is fully working, but sometimes I might just need to commit it to back it up.